package com.practice.web;

import java.io.IOException;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/calc-dynamic")
public class CalcDynamic extends HttpServlet {
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String paramValue = req.getParameter("value");
		String paramOp = req.getParameter("op");
		String paramDot = req.getParameter("dot");
		
		Cookie[] cookies = req.getCookies();
		String exp = "0";
		
		if(cookies != null) {
			for(Cookie cookie : cookies) {
				if(cookie.getName().equals("exp")) {
					exp = cookie.getValue();
					break;
				}
			}
		}
		
		if(paramOp != null)
		{
			if(paramOp.equals("="))
			{
				// JS 코드를 실행할 수 있는 엔진이라고 한다.
				ScriptEngine engine = new ScriptEngineManager().getEngineByName("nashorn");
				try {
					exp = String.valueOf(engine.eval(exp));
				} catch (ScriptException e) {
					e.printStackTrace();
				}
			}
			else if(paramOp.equals("CE") || paramOp.equals("C"))
			{
				exp = "0";
			}
			else if(paramOp.equals("DEL"))
			{
				if(exp.length() > 0)
				{
					exp = exp.substring(0, exp.length() - 1);
				}
				if(exp.equals(""))
				{
					exp = "0";
				}
			}
			else
			{
				if(paramOp.equals("÷"))
				{
					paramOp = "/";
				}
				else if(paramOp.equals("X"))
				{
					paramOp = "*";
				}
				exp += paramOp;
			}
		}
		else if(paramValue != null)
		{
			if(exp.equals("0")) {
				exp = "";
			}
			exp += paramValue;
		}
		else if(paramDot != null)
		{
			exp += paramDot;
		}
		
		
		Cookie expCookie = new Cookie("exp", exp);
		
		resp.addCookie(expCookie);
		resp.sendRedirect("/calc-dynamic.jsp");
	}
}
