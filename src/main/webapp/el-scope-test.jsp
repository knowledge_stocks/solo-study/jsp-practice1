<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
	// pageContext에 Attribute 설정
	pageContext.setAttribute("att1", "page의 att1");
	
	// request에 Attribute 설정
	request.setAttribute("att1", "request의 att1");
	request.setAttribute("att2", "request의 att2");
	
	// session에 Attribute 설정
	session.setAttribute("att1", "session의 att1");
	session.setAttribute("att3", "session의 att3");
	
	// application에 Attribute 설정
	application.setAttribute("att1", "application의 att1");
	application.setAttribute("att4", "application의 att4");
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<h3>Java 소스 부분</h3>
pageContext.setAttribute("att1", "page의 att1");<br>
<br>
request.setAttribute("att1", "request의 att1");<br>
request.setAttribute("att2", "request의 att2");<br>
<br>
session.setAttribute("att1", "session의 att1");<br>
session.setAttribute("att3", "session의 att3");<br>
<br>
application.setAttribute("att1", "application의 att1");<br>
application.setAttribute("att4", "application의 att4");<br>
<br>
<h3>참조 결과</h3>
\${att1}: ${att1}<br>
\${att2}: ${att2}<br>
\${att3}: ${att3}<br>
\${att4}: ${att4}<br>
<br>
\${requestScope.att1}: ${requestScope.att1}<br>
\${sessionScope.att1}: ${sessionScope.att1}<br>
\${applicationScope.att1}: ${applicationScope.att1}<br>
<br>
<h3>결론</h3>
EL은 page -> request -> session -> application 순으로 Attribute를 참조한다.<br>
만약 특정 Scope의 데이터를 참조하고 싶다면 xxxScope.&lt;attrbuteName&gt;으로 참조할 수 있다.<br>
<br>
<h3>그 외에도..</h3>
param: 파라미터를 맵 형태로 가지고 있음<br>
paramValues: 파라미터 값들을 배열 형태로 가지고 있음<br>
header: 헤더 정보<br>
headerValues: 해더의 갑들을 배열 형태로<br>
cookie: 쿠키 정보<br>
initParam: 컨텍스트 초기화 파라미터들<br>
<br>
\${empty param.param1}: ${empty param.param1}<br>
\${param.param1}: ${param.param1}<br>
\${empty param.param1?'param1이 비어 있습니다.':param.param1}: ${empty param.param1?'param1이 비어 있습니다.':param.param1}<br>
\${header.host}: ${header.host}<br>
\${header["host"]}: ${header["host"]}<br>
</body>
</html>
<%
	// session과 application의 Attributes는 페이지가 종료되어도 유지되므로 삭제해 줌
	session.removeAttribute("att1");
	session.removeAttribute("att3");
	
	application.removeAttribute("att1");
	application.removeAttribute("att4");
%>